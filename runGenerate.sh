#!/bin/bash
set -e

current_path=$(dirname "$0")
geth="$current_path"/geth
accounts_file_path="$current_path"/accounts.csv
keystore="$current_path"/keystore/
password_length=16

rand() {
  chars=$"!$%&*@0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
  password=""
  # shellcheck disable=SC2004
  for ((i = 0; i < $password_length; i++)); do
    password+="${chars:$((RANDOM % ${#chars})):1}"
  done
  echo "$password"
}

gen() {
  # shellcheck disable=SC2004
  for ((i = 0; i < $num; i++)); do
    # shellcheck disable=SC2006
    password=$(rand)
    # shellcheck disable=SC2006
    log=$($geth account new --keystore "$keystore" --password <(echo "$password"))
    echo "$log"
    write "$log" "$password"
  done
}

write() {
  log=$1
  password=$2
  # shellcheck disable=SC2006
  addr=$(echo "$log" | grep -Eo "0x[a-fA-F0-9]{40}")
  # shellcheck disable=SC2006
  private_key_file_name=$(echo "$log" | grep "Path of the secret key file" | awk -F '[/]' '{print $(NF-0)}')
  echo "$addr,$password,$private_key_file_name" >>"$accounts_file_path"
}

# shellcheck disable=SC2162
read -p "Please input how many addresses to generate: " num
gen num
